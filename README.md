# How to: ESP32 Development With Arduino IDE

A brief guide on ESP32 development using Arduino IDE and tools.

Holger Zahnleiter, 2021-11-13

## Introduction

Only now and then do I do ESP32 and ESP8266 development.
As a result, I have no routine in ESP development.
It is almost like starting from scratch.
Hence, here is a small guide to remind myself on how to develop for ESP32/8266 using Arduino IDE and tools.

## Requirements

For this guide you need:

- Arduino IDE 1.8.16 (https://www.arduino.cc/en/software)
- Arduino ESP32 add-on from Espressif (https://dl.espressif.com/dl/package_esp32_index.json)
- NodeMCU ESP32 module
- Micro-USB cable
- CP210x driver
  - Windows https://www.silabs.com/documents/public/software/CP210x_Universal_Windows_Driver.zip
  - macOS https://www.silabs.com/documents/public/software/Mac_OSX_VCP_Driver.zip
  - Linux, already part of the kernel

<img src="./nodemcu_esp32_board.jpeg"/>

## Install Driver for USB to Serial Bridge (CP210x)

My ESP32 board is equipped with an USB to serial bridge from Silicon Labs.
A driver for this is part of Linux already.
No need to install a driver.
However, non-Linux users download the driver from the URL given above.
Follow the instructions given by the installer.

## Installing ESP32 Add-On

I am assuming, you already have the Arduino IDE installed.
Start the Arduino IDE.
Under preferences add this URL as an additional board manager URL: https://dl.espressif.com/dl/package_esp32_index.json

<img src="./add_esp32_board.png"/>

Now, from the tools menu open the board manager.
There, search for the ESP32 add-on.
Select it and click "Install".

<img src="./install_esp32_board.png"/>

Depending on your network bandwith you should be good to go quickly.

## Uploading Your Programms to an ESP32 Board

Connect your ESP32 development board to one of your computer's USB ports.
A micro USB cable is required for this.
(This depends in fact of the ESP32 hardware you are using.)

From the file menu pick a most simple example: "AnalogReadSerial".

From the tools menu pick a board.
I am using an NodeMCU ESP32, therfore I am picking "NodeMCU-32S".

Also from the tools menu pick a port, to which your ESP32 is connected to.
This depends on the operating system a lot.
I am picking "/dev/cu.SLAB_USBtoUART" as I am using macOS.

<img src="./board_and_port.png"/>

My ESP32 board is equipped with an USB to serial bridge from Silicon Labs.
I had the corresponding driver installed already because many microcontroller boards are equipped with this widespread chip.
You may need to install that driver if it is not installed on your machine already.

Click the "Compile and Upload" button.
This compiles the code and - if correct - uploads the binary to the ESP32.

<img src="./compile_and_upload.png"/>

The program gets executed immediately after uploading.
To verify this, choose "Serial Monitor" from the "Tools" menu.
This should work right out of the box.
Program and monitor are both using default settings for the USB-serial connection.
They should not have problems meeting each other.

<img src="./serial_monitor.png"/>

Originally I was picking "ESP32 Dev Module" as my board.
This also worked fine.

## Differences Between Windows and macOS

On Windows 7 I had to press the NodeMCUs "BOOT" button so that uploading could happen.
And, as displayed on screen, I had to press "RST" after upload to start my newly uploaded program.

On macOS this all happens automatically.

I have no idea where this difference comes from or if others experience the same. Might be due to my Windows machine and OS version are relatively old?
